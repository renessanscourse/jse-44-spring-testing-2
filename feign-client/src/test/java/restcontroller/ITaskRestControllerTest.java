package restcontroller;

import feign.Client;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.rest.controller.IAuthenticationRestController;
import ru.ovechkin.tm.rest.controller.IProjectRestController;
import ru.ovechkin.tm.rest.controller.ITaskRestController;

import java.net.CookieManager;
import java.net.CookiePolicy;

public class ITaskRestControllerTest {

    final CookieManager cookieManager = new CookieManager();
    {
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    }

    final okhttp3.OkHttpClient.Builder builder =
            new okhttp3.OkHttpClient().newBuilder();
    {
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
    }

    final Client client = new OkHttpClient(builder.build());

    final IAuthenticationRestController auth = IAuthenticationRestController.client(client);

    {
        auth.login("user", "user");
    }

    @Rule
    public final ErrorCollector errorCollector = new ErrorCollector();

    final Project projectWithTasks = new Project();

    {
        IProjectRestController.client(client).create(projectWithTasks);
    }

    @Test
    public void findAllTest() {
        Assert.assertTrue(ITaskRestController.client(client).show(projectWithTasks.getId()).isEmpty());
    }

    @Test
    public void createTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignWithProject");
        task.setDescription("FromFeignWithProject");
        ITaskRestController.client(client).create(projectWithTasks.getId(), task);
        try {
            Assert.assertTrue(ITaskRestController.client(client).show(projectWithTasks.getId()).contains(task));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
    }

    @Test
    public void removeTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignForRemove");
        task.setDescription("FromFeignForRemove");
        ITaskRestController.client(client).create(projectWithTasks.getId(), task);
        ITaskRestController.client(client).remove(task.getId(), projectWithTasks.getId());
        Assert.assertFalse(ITaskRestController.client(client).show(projectWithTasks.getId()).contains(task));
    }

    @Test
    public void editTest() {
        final Task task = new Task();
        task.setProject(projectWithTasks);
        task.setName("FromFeignForEdit");
        task.setDescription("FromFeignForEdit");
        ITaskRestController.client(client).create(projectWithTasks.getId(), task);
        final Task taskEdited = new Task();
        taskEdited.setProject(projectWithTasks);
        taskEdited.setName("EditedFromFeignForEdit");
        taskEdited.setDescription("EditedFromFeignForEdit");
        ITaskRestController.client(client).edit(taskEdited, projectWithTasks.getId(), task.getId());

        Assert.assertTrue(ITaskRestController.client(client).show(projectWithTasks.getId()).contains(taskEdited));
        ITaskRestController.client(client).remove(task.getId(), projectWithTasks.getId());
    }
}

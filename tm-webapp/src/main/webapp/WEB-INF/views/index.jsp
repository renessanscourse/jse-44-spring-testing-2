<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="resources/_prepage.jsp"/>

    <body style="text-align: center">

        <sec:authorize access="isAuthenticated()">
            <a href="/projects/all"><button class="customButton">GO TO PROJECTS</button></a>
        </sec:authorize>

        <sec:authorize access="!isAuthenticated()">
            <a href="/login"><button class="customButton">LOGIN</button></a>
        </sec:authorize>

        <sec:authorize access="!isAuthenticated()">
            <a href="/user/registryForm"><button class="customButton">REGISTRY</button></a>
        </sec:authorize>
    </body>

</html>

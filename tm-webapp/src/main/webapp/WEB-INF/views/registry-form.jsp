<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="resources/_prepage.jsp"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <body style="text-align: center">

        <h1>REGISTER A NEW ACCOUNT</h1>

        <form:form method="post" action="/user/register" modelAttribute="user">
            <div style="margin-top: 10px">
                <form:label path="login">Login</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="login"/>
            </div>
            <div style="margin-top: 10px">
                <form:label path="passwordHash">Password</form:label>
            </div>
            <div style="margin-top: 10px">
                <form:input path="passwordHash"/>
            </div>
            <div>
                <br>
                <input type="submit" value="Registry" class="customButton">
            </div>
        </form:form>

    </body>

</html>
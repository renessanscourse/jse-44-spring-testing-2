package ru.ovechkin.tm.exeption.empty;

public class ProjectListEmptyException extends RuntimeException {

    public ProjectListEmptyException() {
        super("Error! You don't have any projects...");
    }

}
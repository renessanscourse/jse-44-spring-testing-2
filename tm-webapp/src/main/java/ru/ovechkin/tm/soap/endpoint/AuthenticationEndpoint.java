package ru.ovechkin.tm.soap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.api.service.IUserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AuthenticationEndpoint {


    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @WebMethod
    public String login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) {
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return ("success status: " + authentication.isAuthenticated());
    }

    @WebMethod
    public ru.ovechkin.tm.entity.User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userService.findByLogin(username);
    }

    @WebMethod
    public String logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return ("success status: " + SecurityContextHolder.getContext().getAuthentication());
    }

}
package ru.ovechkin.tm.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.util.UserUtil;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class ProjectRestControllerTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc =
                MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test_user");
        user.setPasswordHash(passwordEncoder.encode("test_user"));
        user.setRole(Role.USER);
        userRepository.save(user);
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test_user", "test_user");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void testAllProjects() throws Exception {
        final Project project = new Project();
        project.setName("testAllProjects");
        project.setDescription("testAllProjects");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/rest/projects/all"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem(project.getName())));
    }

    @Test
    public void testCreate() throws Exception {
        final Project project = new Project();
        project.setName("testCreate");
        project.setDescription("testCreate");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(project);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/rest/projects/create")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem(project.getName())));
    }

    @Test
    public void testRemove() throws Exception {
        final Project project = new Project();
        project.setName("testRemove");
        project.setDescription("testRemove");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .delete("/rest/projects/remove")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    public void testEdit() throws Exception {
        final Project project = new Project();
        project.setName("testEdit");
        project.setDescription("testEdit");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        projectRepository.save(project);
        final Project projectChanged = new Project();
        projectChanged.setName("EDITED");
        projectChanged.setDescription("EDITED");
        project.setUser(userRepository.findByLogin(UserUtil.getUser().getUsername()));
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(projectChanged);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post("/rest/projects/edit")
                                .param("id", project.getId())
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(requestJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem(projectChanged.getName())));
    }

}